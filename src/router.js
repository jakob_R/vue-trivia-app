import VueRouter from 'vue-router';
import StartScreen from './components/Views/StartScreen'
import TriviaGame from './components/Views/TriviaGame'
import Results from './components/Views/Results'


const routes = [
    {
        path: '/',
        name: 'startScreen',
        component: StartScreen
    },
    {        
        path: '/triviaGame',
        name: 'TriviaGame',
        component: TriviaGame
    },
    {
        path:'/Results',
        name: 'Results',
        component: Results,
        props: true
    }
]

const router = new VueRouter({routes});

export default router;