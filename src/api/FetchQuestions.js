export function fetchQuestions() {
  return fetch("https://opentdb.com/api.php?amount=10&difficulty=easy")
  .then(response  => response.json())
  .then(json => {
    return json.results
  })
}
